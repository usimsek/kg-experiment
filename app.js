var walker = require("walker");
var fs = require("fs");
var jsonld = require("jsonld");
var path = require("path");
var graphDB = require("./graphdb.js");
var cayleyDB = require("./cayleydb.js");
var n3 = require("n3");





walker("feratel\\2017.02.23\\accommodation").on("file", function(entry, stat) {
fs.readFile(entry, readFileCallback.bind(null, entry));

  //console.log("pushed " + entry);
});

function readFileCallback(entry, err, data) {
  var dataJSON = JSON.parse(data);
  dataJSON["@id"] = "urn:tkg:" + path.basename(entry).split(".")[0];

  jsonld.toRDF(
    dataJSON,
    { format: "application/nquads" },
    function(err, nquads) {
      if (err) {
        console.error(err);
        return false;
      }
      var validQuads = n3.Parser().parse(nquads);
      if (validQuads)
        graphDB.populateGraphDB(nquads);
    }
  );
}
