var request = require("request");
var aSync = require("async");


var queue = aSync.queue(
  function(nquads, callback) {
    var headers = {
      "Content-Type": "text/x-nquads"
    };

    var options = {
      url: "http://localhost:7200/repositories/tkg-exp/statements",
      method: "POST",
      headers: headers,
      body: nquads
    };

    request(options, function(error, response, body) {
      if (!error) {
        callback();
        console.log("uploaded  . Remaning: " + queue.length());
      }
      else
      {
        console.log(error);
      }
    });

  },
  1
);

queue.drain = function() {
  if (queue.length() == 0) console.log("all requests done");
};

module.exports = {
  populateGraphDB: function(nquads) {
    queue.push(nquads);


  }
};
