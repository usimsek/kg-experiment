var n3 = require("n3");
var cayley = require("cayley");

module.exports = {

  populateCayley : function (err, nquads) {
    if (err) {
      console.error(err);
      return false;
    }

    var cayleyInput = convertNquadsToCayleyInput(nquads);
    var cayleyClient = cayley("http://localhost:64210");
    cayleyClient.write(cayleyInput, function(err, body, res) {
      console.log(body);
    });
  },

  convertNquadsToCayleyInput: function (nquads) {
    var cayleyInput = [];

    var parser = n3.Parser();
    var quads = parser.parse(nquads);

    //var jsonQuads = JSON.parse(quads);
    for (var i = 0; i < quads.length; i++) {
      var tripleJSON = {};
      tripleJSON["subject"] = quads[i]["subject"];
      tripleJSON["predicate"] = quads[i]["predicate"];
      tripleJSON["object"] = quads[i]["object"];
      cayleyInput.push(tripleJSON);
    }
    return cayleyInput;
  }

};
